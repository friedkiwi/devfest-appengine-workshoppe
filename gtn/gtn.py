#!/usr/bin/env python2.7

import cherrypy
import random

class GuessTheNumber(object):
  def __init__(self):
    self.reset()

  def reset(self):
    self.number = random.randrange(1, 10)
    print 'RESET', self.number

  def index(self, guess=0):
   guess = int(guess)
   if guess > self.number:
     print 'Too Large'
     msg = "Too large"
   elif guess < self.number and guess > 0:
     print 'Too Small'
     msg = "Too small."
   elif guess == self.number:
     print 'Congratulations'
     msg = "Congratulations !"
     self.reset()
   else:
     msg = "Chose a number [1, 10]."
     return msg + "<br/>" + "<form><input name='guess' /></form>"
  index.exposed = True

cherrypy.quickstart(GuessTheNumber())
